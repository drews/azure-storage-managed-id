# Operating on Azure Blob Storage with Managed Identities

## Prereqs

### Managed Identity

Create a 'Managed Identity'.  This will be a special user stored in Azure.

### Storage Account

Grant your newly created Managed Identity the proper roles.

For this example, I needed to grant: 

`Storage Account Contributor` - This lets you create containers and such in the service account

`Storage Blob Data Contributor` - This lets you edit the actual blob contents

### VM Identity

Under `Identity` -> `User Assigned`, add your newly created Managed Identity

## Authenticating

Once the prereqs are complete, you can use an auth token from a private URL on
the VM

```
http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https%3A%2F%2Fmanagement.azure.com%2F
```

The proper call to this URL will give you an authentication token that can be
signed and used to make operations to the storage account

## Example Scripts

`example_scripts/auth_and_operate_with_requests.py` - This walks through all of
the steps needed to authenticate using just web calls.  Don't actually use this
unless you like pain, but it can be a good reference for what happens under the
covers

`example_scripts/upload_file_with_sdk.py` - This uses the Azure SDK to upload a
file.  It's a lot easier and cleaner to use this for operations

## Helpful URLs

* https://docs.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/tutorial-windows-vm-access-storage-sas
