#!/usr/bin/env python3

import requests
import sys
from datetime import datetime, timedelta


def main():
    az_token_url = "http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https%3A%2F%2Fmanagement.azure.com%2F"
    storage_account = "drewstoragetest"
    blob_container = "blobbyblob"
    subscription_id = "1f0714f3-ad90-44a8-91cd-f58d8083895e"
    resource_group = "drewtest"
    filename = "foo.txt"

    token_r = requests.get(az_token_url, headers={"Metadata": "true"})
    access_token = token_r.json()["access_token"]

    blob_headers = {
        "x-ms-version": "2017-11-09",
        "Authorization": f"Bearer {access_token}",
    }

    now = datetime.now()
    expire = now + timedelta(hours=1)

    sas_params = {
        "canonicalizedResource": f"/blob/{storage_account}/{blob_container}",
        # The kind of resource accessible with the SAS, in this case a container (c).
        "signedResource": "c",
        # Permissions for this SAS, in this case (r)ead, (c)reate, and (w)rite.
        # Order is important.
        "signedPermission": "rcw",
        "signedProtocol": "https",
        # UTC expiration time for SAS in ISO 8601 format, for example
        # 2017-09-22T00:06:00Z.
        "signedExpiry": "%sZ" % expire.isoformat(timespec="seconds"),
    }

    sas_r = requests.post(
        f"https://management.azure.com/subscriptions/{subscription_id}/resourceGroups/{resource_group}/providers/Microsoft.Storage/storageAccounts/{storage_account}/listServiceSas/?api-version=2017-06-01",
        json=sas_params,
        headers={"Authorization": f"Bearer {access_token}"},
    )
    sas_token = sas_r.json()["serviceSasToken"]

    blob_content = "Oh Hai Drew"
    blob_headers = {
        "x-ms-version": "2015-02-21",
        "x-ms-date": now.isoformat(timespec="seconds"),
        "Content-Type": "text/plain; charset=UTF-8",
        "x-ms-blob-content-disposition": f'attachment; filename="{filename}"',
        "x-ms-blob-type": "BlockBlob",
        "x-ms-meta-m1": "v1",
        "x-ms-meta-m2": "v2",
        "Content-Length": "%s" % (len(blob_content)),
    }
    blob_r = requests.put(
        f"https://{storage_account}.blob.core.windows.net/{blob_container}/myblockblob?{sas_token}",
        headers=blob_headers,
        data=blob_content,
    )
    blob_r.raise_for_status()
    print("Success! 👍")


if __name__ == "__main__":
    sys.exit(main())
