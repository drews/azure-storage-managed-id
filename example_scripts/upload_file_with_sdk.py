#!/usr/bin/env python3

import sys
from azure.storage.blob import BlobServiceClient
from azure.identity import DefaultAzureCredential


def main():
    storage_account = "drewstoragetest"
    blob_container = "bobtheblob2"

    # service = BlockBlobService(account, token_credential=get_token())
    credential = DefaultAzureCredential()
    service = BlobServiceClient(
        f"{storage_account}.blob.core.windows.net", credential=credential
    )
    container_names = []
    for i in service.list_containers():
        container_names.append(i["name"])

    print(container_names)
    if blob_container not in container_names:
        service.create_container(blob_container)

    upload_file = "/etc/hosts"
    blob_client = service.get_blob_client(container=blob_container, blob="test_update")
    with open(upload_file, "rb") as data:
        blob_client.upload_blob(data)


if __name__ == "__main__":
    sys.exit(main())
